<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_devis' => 'Ajouter ce devis',

	// C
	'champ_descriptif_label' => 'Descriptif',
	'champ_reference_label' => 'Référence',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_devis' => 'Confirmez-vous la suppression de ce devis ?',

	// I
	'icone_creer_devis' => 'Créer un devis',
	'icone_modifier_devis' => 'Modifier ce devis',
	'info_1_devis' => 'Un devis',
	'info_aucun_devis' => 'Aucun devis',
	'info_devis_auteur' => 'Les devis de cet auteur',
	'info_nb_devis' => '@nb@ devis',

	// R
	'retirer_lien_devis' => 'Retirer ce devis',
	'retirer_tous_liens_devis' => 'Retirer tous les devis',

	// S
	'statut_accepte' => 'accepté',
	'supprimer_devis' => 'Supprimer ce devis',

	// T
	'texte_ajouter_devis' => 'Ajouter un devis',
	'texte_changer_statut_devis' => 'Ce devis est :',
	'texte_creer_associer_devis' => 'Créer et associer un devis',
	'texte_definir_comme_traduction_devis' => 'Ce devis est une traduction du devis numéro :',
	'titre_devis' => 'Devis',
	'titre_devis_rubrique' => 'Devis de la rubrique',
	'titre_langue_devis' => 'Langue de ce devis',
	'titre_logo_devis' => 'Logo de ce devis',
	'titre_objets_lies_devis' => 'Liés à ce devis',
);
